﻿using System;
using static System.Console;

namespace HomeWorkNumberOne
{
    class Program
    {
        static void Main(string[] args)
        {
            TaskOne();
            TaskTwo();
        }

        /// <summary>
        /// Выводид в консоль "Hello World!" благодаря WriteLine
        /// </summary>
        static void TaskOne()
        {
            WriteLine("Вывод в консоль благодаря WriteLine");
            WriteLine("Hello World!");
            Console.ReadKey();
        }
        /// <summary>
        /// Выводид в консоль "Hello World!!!" благодаря Write
        /// </summary>
        static void TaskTwo()
        {
            WriteLine("Вывод в консоль благодаря Write");
            Write("Hello ");
            Write("World");
            Write("!!!\n");
            WriteLine("Нажмите на Enter для завержения программы.");
            ReadLine();
        }
    }
}
